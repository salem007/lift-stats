/*
  DENDOGRAM
  ---------
  12.2019
  Tree graph powered by D3

*/

function wrap(text, width) { // because SVG text functionality sucks
    text.each(function () {
        var text = d3.select(this),
            words = text.text().split(/\s+/).slice(0,4).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.1, // ems
            x = text.attr("x"),
            y = text.attr("y"),
            dy = 0, //parseFloat(text.attr("dy")),
            tspan = text.text(null)
                        .append("tspan")
                        .attr("x", x)
                        .attr("y", y)
                        .attr("dy", dy + "em");
        if (words.length < 2){
          return text.text(words[0]);
        } // end if
        var line_num = 0;
        while (word = words.pop()) {
            if ( word == "Mountain" ) {
              word = "Mtn";
            } // end if
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width & line_num++ > 0) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan")
                            .attr("x", x)
                            .attr("y", y)
                            .attr("dy", ++lineNumber * lineHeight + dy + "em")
                            .text(word);
            } // end if
        } // end while(words)
    }); // end text.each
} // end wrap()


function dendogram(container_ID,data_source,value_field,chart_options){ 

  // unpack chart options (this is likely not best practices)
  var display_threshold=null, tooltip_text=null, controls_ID=null;
  if (chart_options != null ){ 
    if ( typeof chart_options.display_threshold !== 'undefined' )
      display_threshold = chart_options.display_threshold;
    if ( typeof chart_options.tooltip_text !== 'undefined' )
      tooltip_text = chart_options.tooltip_text;
    if ( typeof chart_options.controls_ID !== 'undefined' )
      controls_ID = chart_options.controls_ID;
  }

  // set the dimensions and margins of the graph
  var container = d3.select(container_ID);
  var margin = {top: 5, right: 5, bottom: 5, left: 5},
    width = container.attr("width") - margin.left - margin.right,
    height = container.attr("height") - margin.top - margin.bottom;

  container.classed("dendogram",true);

  // append the svg object to the body of the page
  var svg = d3.select(container_ID)
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

  var treemap = d3.treemap()
    .size([width, height])
    .padding(2);

  // Define the div for the tooltip
  if( value_field != null ){
    var tooltip = d3.select(container_ID).append("div") 
      .attr("class", "tooltip")       
      .style("opacity", 0);
  }

  // read json data
  d3.json(data_source, function(data) {

    // Give the data to this cluster layout:
    var root = d3.hierarchy(data,d=>d.children).sum(d=>d[value_field]);
    var groups = root.children.map(c=>c.data.name);

    // prepare a color scale
    var color = d3.scaleOrdinal()
      .domain(groups)
      .range(['#4a0075','#cc4400','#00612d','#c20000','#0052a3','#ad0062','orange','#009fb8','#333']);

    // -- things from here down require y-value data
    treemap(root);

    // set up opacity based on lift count
    var max_value = d3.max(root.leaves().map(d=>d.value));
    var opacity = d3.scaleSqrt()
      .domain([1, max_value*1.2])
      .range([.3,1])

    // use this information to add rectangles:
    var nodes = svg
      .selectAll("rect")
      .data(root.leaves())
      .enter()
      .append("rect")
        .attr('x',d=>d.x0)
        .attr('y',d=>d.y0)
        .attr('width',d=>(d.x1 - d.x0))
        .attr('height',d=>(d.y1 - d.y0))
        .style("fill", d=>color(d.parent.data.name))
        .style("opacity",d=>opacity(d.value))
        .on("mouseover",function(d){
          
          d3.select(this).style("opacity",opacity(d.value*1.4));

          if( tooltip_text != null){
            tooltip.transition()    
                .duration(200)    
                .style("opacity", .9);
            tooltip.html(tooltip_text(d))
                .style("left", (d3.event.pageX) + "px")   
                .style("top", (d3.event.pageY - 18) + "px");  
          } // end tooltip if

        }) // end mouseover
        .on("mouseout",function(d){

          d3.select(this).style("opacity",opacity(d.value));

          if( tooltip_text != null){
            tooltip.transition()    
                .duration(500)    
                .style("opacity", 0); 
          } // end tooltip if

        }); // end mouseout

    // and to add the text labels
    var labels = svg
      .selectAll("text")
      .data(root.leaves())
      .enter()
      .append("text")
        .attr("x", d=>(d.x0+2))
        .attr("y", d=>(d.y0+12))
        .text(d=>( d.value >= display_threshold ? d.data.name : '' ))
        .attr("font-size", "10px")
        .attr("fill", "white");

    if ( controls_ID != null ){ // update functionality

      d3.selectAll(controls_ID + ' li')
        .on("click",function(){

          var self = d3.select(this);

          if(!self.classed("selected")){

            d3.selectAll(controls_ID + ' li').classed("selected",false);
            self.classed("selected",true);

            // update treemap with new value field
            value_field = self.attr("value_field");
            root = d3.hierarchy(data,d=>d.children).sum(d=>d[value_field]);
            treemap(root);

            // udpate opacity scale
            max_value = d3.max(root.leaves().map(d=>d.value));
            opacity.domain([1, max_value*1.2]);

            // redraw nodes
            nodes
              .data(root.leaves())
              .transition()
                .duration(1500)
                .attr('x',d=>d.x0)
                .attr('y',d=>d.y0)
                .attr('width',d=>(d.x1 - d.x0))
                .attr('height',d=>(d.y1 - d.y0))
                .style("fill", d=>color(d.parent.data.name))
                .style("opacity",d=>opacity(d.value));

            // Redraw labels
            display_threshold = self.attr("display_threshold");
            labels
              .data(root.leaves())
              .transition()
                .duration(1500)
                .text(d=>( d.value >= display_threshold ? d.data.name : '' ))
                .attr("x", d=>(d.x0+2))
                .attr("y", d=>(d.y0+12));
          } // end selected if

        }); // end click function

    } // end update if

  }); // end json load

} // end dendogram()